namespace PlaycoinPay.Domain
{
    public enum PaymentMethod
    {
        COIN,
        CARD,
        BANK,
        PHONE
    }
}