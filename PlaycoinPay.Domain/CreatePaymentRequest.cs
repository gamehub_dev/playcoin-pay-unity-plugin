﻿using System;

namespace PlaycoinPay.Domain
{
    public class CreatePaymentRequest
    {
        //거래방법
        public PaymentMethod PaymentMethod { get; }
        //게임에서 결제를 진행한 사용자 id
        public string TradePayerId { get; }
        //게임에서 생성한 거래번호, 게임별로 반드시 unique 해야함
        public string TradeNo { get; }
        //결제상품명
        public string TradeItemName { get; }
        //결제금액
        public double TradeAmount { get; }
        //게임내 아이템 거래일시 'yyyy-MM-dd'T'HH:mm:ssXXX'
        public DateTime TradeTime { get; }

        public CreatePaymentRequest(PaymentMethod paymentMethod, string tradePayerId, string tradeNo, string tradeItemName, double tradeAmount, DateTime tradeTime)
        {
            PaymentMethod = paymentMethod;
            TradePayerId = tradePayerId ?? throw new ArgumentNullException(nameof(tradePayerId));
            TradeNo = tradeNo ?? throw new ArgumentNullException(nameof(tradeNo));
            TradeItemName = tradeItemName ?? throw new ArgumentNullException(nameof(tradeItemName));
            TradeAmount = tradeAmount;
            TradeTime = tradeTime;
        }
    }
}