using System.Collections.Generic;

namespace PlaycoinPay.Domain
{
    public class CreatePaymentResponse : Result
    {
        public Result Result { get; set; }
        //status code 가 200 이 아닌 경우 오류 메세지
        public List<string> Messages { get; set; }
    }
}