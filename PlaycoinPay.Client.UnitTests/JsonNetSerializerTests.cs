﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using PlaycoinPay.Client.Utils;
using PlaycoinPay.Domain;
using RestSharp;
using Xunit;

namespace PlaycoinPay.Client.UnitTests
{
    public class JsonNetSerializerTests
    {
        private readonly JsonNetSerializer _serializer;

        public JsonNetSerializerTests()
        {
            _serializer = new JsonNetSerializer();
        }
        
        [Fact]
        public void DateFormatTest()
        {
            TimeZoneInfo aestZone = TimeZoneInfo.FindSystemTimeZoneById(@"Australia/Queensland");
            var dateTimeOffset = new DateTimeOffset(
                new DateTime(2019, 1, 1,
                    13, 30, 45, 500, 
                    DateTimeKind.Unspecified), aestZone.BaseUtcOffset); 
            
            string dateTimeStr = _serializer.Serialize(dateTimeOffset);

            
            dateTimeStr.Should().Be("\"2019-01-01T01:30:45+10:00\"");
        }
        
        [Fact]
        public void CamelCaseTest()
        {
            string camelCaseStr = _serializer.Serialize(new { TradePayerId = "100"});

            camelCaseStr.Should().Be("{\"tradePayerId\":\"100\"}");
        }

        [Fact]
        public void DeserializeTest()
        {
            var expResponse = new CreatePaymentResponse();
            expResponse.Result = new Result { PaymentId = 333 };
            var resStr = _serializer.Serialize(expResponse);
            
            
            var res = new RestResponse();
            res.Content = resStr;
            CreatePaymentResponse cratePaymentResponse = _serializer.Deserialize<CreatePaymentResponse>(res);
            
            cratePaymentResponse.Should().BeEquivalentTo(expResponse);
        }
    }
}