using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using PlaycoinPay.Domain;
using RestSharp;
using Xunit;

namespace PlaycoinPay.Client.UnitTests
{
    public class PaymentHttpClientTests
    {
        private readonly PaymentHttpClient _client;

        public PaymentHttpClientTests()
        {
            _client = new PaymentHttpClient("testAccessKey", "A000000001");
        }

        [Fact]
        public async Task CreatePaymentAsync_Should_Return_Success()
        {
            var paymentRequest = new CreatePaymentRequest(
                PaymentMethod.COIN,
                "donam",
                Guid.NewGuid().ToString(),
                "test-product-01",
                1000.50,
                DateTime.Now
            );

            CreatePaymentResponse res = await _client.CreatePaymentAsync(paymentRequest);

            
            res.Result.Should().NotBeNull();
            res.Result.PaymentId.Should().BeGreaterThan(0);
            res.Messages.Should().BeNull();
        }
    }
}