﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PlaycoinPay.Client.Utils;
using PlaycoinPay.Domain;

namespace PlaycoinPay.Client
{
    public class PaymentHttpClient
    {
        private readonly HttpClient _client;
        private readonly RequestFactory _requestFactory;
        
        public PaymentHttpClient(string accessKey, string applicationId)
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _requestFactory = new RequestFactory(accessKey, applicationId);
            
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateFormatString = "yyyy-MM-ddThh:mm:ssK"
            };
        }
        
        public async Task<CreatePaymentResponse> CreatePaymentAsync(CreatePaymentRequest paymentRequest)
        {
            HttpRequestMessage request = _requestFactory.GenerateCreateRequest(paymentRequest);

            HttpResponseMessage res = await _client.SendAsync(request);

            var content = await res.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CreatePaymentResponse>(content);
        }
    }
}