using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PlaycoinPay.Domain;

namespace PlaycoinPay.Client.Utils
{
    public class RequestFactory
    {
        private readonly string _baseUrl = "http://dev-payment-api-alhk.playcoin.me//api/v1/payments/";
        
        private readonly string _accessKey;
        private readonly string _applicationId;

        public RequestFactory(string accessKey, string applicationId)
        {
            _accessKey = accessKey;
            _applicationId = applicationId;
        }
        
        public HttpRequestMessage GenerateCreateRequest(CreatePaymentRequest paymentRequest)
        {
            var request = CreateBaseRequestMessage();
            request.RequestUri  = new Uri(_baseUrl + "create");
            request.Content = new StringContent(JsonConvert.SerializeObject(paymentRequest), Encoding.UTF8, "application/json");

            return request;
        }

        private HttpRequestMessage CreateBaseRequestMessage()
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.Headers.Add("accessKey", _accessKey);
            request.Headers.Add("aid", _applicationId);

            return request;
        }
    }
}