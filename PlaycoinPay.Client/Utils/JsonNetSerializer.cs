using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Serialization;

namespace PlaycoinPay.Client.Utils
{
    public class JsonNetSerializer : IRestSerializer
    {
        public string ContentType { get; set; } = "application/json";
        public string[] SupportedContentTypes { get; } =
        {
            "application/json", "text/json", "text/x-json", "text/javascript", "*+json"
        };
        public DataFormat DataFormat { get; } = DataFormat.Json;

        private readonly JsonSerializerSettings _serializerSettings = 
            

        public string Serialize(object obj) =>
            JsonConvert.SerializeObject(obj, _serializerSettings);

        public string Serialize(Parameter parameter) =>
            JsonConvert.SerializeObject(parameter.Value, _serializerSettings);

        public T Deserialize<T>(IRestResponse response) =>
            JsonConvert.DeserializeObject<T>(response.Content);
    }
}